﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonModels.Invoice;

namespace InvoiceComponent
{
    public static class TaxInv 
    {
        private static TaxCardX.GoldTax _tax;
        public static string ErrCode { get { return _tax.RetCode.ToString(); }}
        public static string ErrString { get { return _tax.RetMsg; } }
        #region 初始化金税盘
        internal static void InitTaxCard(string certPass)
        {
            if(_tax !=null)
            {
                try
                {
                    _tax.CloseCard();
                }
                catch
                {
                }
            }
            try
            {
                _tax = new TaxCardX.GoldTax();
                _tax.CertPassWord = certPass;
                _tax.OpenCard();
                if (_tax.RetCode != 1011)
                {
                    ErrCode = _tax.RetCode.ToString();ErrString = _tax.RetMsg;
                    throw new Exception("开启金税盘失败！\r\n错误代码:" + _tax.RetCode + "\r\n错误信息:" + _tax.RetMsg);
                }
                    
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion
        #region 读新发票号码
        internal static InvStockModel GetInvCode( short invType)
        {
            try
            {
                if (null == _tax)
                    throw new Exception("未初始化！");
                _tax.InfoKind = invType;
                _tax.GetInfo();
                if (_tax.RetCode != 3011)
                {
                    throw new Exception("读取发票信息失败！\r\n错误代码:" + _tax.RetCode + "\r\n错误信息:" + _tax.RetMsg);
                }
                InvStockModel stock = new InvStockModel
                {
                    InvCode = _tax.InfoTypeCode,
                    InvNumber = _tax.InfoNumber,
                    StockCount = _tax.InvStock
                };
                return stock;
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion
        //开发票
        internal void GetInvoice( InvInputModel inv)
        {
            try
            {                
                if (null == _tax)
                    throw new Exception("未初始化！");
                if (string.IsNullOrEmpty(_tax.InfoTypeCode))
                    throw new Exception("请先获取发票号！");
                if (inv.g.Count < 1)
                    throw new Exception("明细行不能为空！");
                string invCode = t._tax.InfoTypeCode;
                int invNumber = t._tax.InfoNumber;
                short invType = t._tax.InfoKind;
                t._tax.InvInfoInit();
                t._tax.InfoClientName = inv.CustName;
                t._tax.InfoClientTaxCode = inv.CustTaxCode;
                t._tax.InfoClientBankAccount = inv.CustBank;
                t._tax.InfoClientAddressPhone = inv.CustAddress;
                t._tax.GoodsListFlag = inv.Goods.Count > 7 ? (short)1 : (short)0;
                t._tax.InfoTaxRate = inv.TaxRate;
                t._tax.InfoNotes = inv.Remark;
                t._tax.InfoCashier = inv.Cashier;
                t._tax.InfoInvoicer = inv.Invoicer;
                t._tax.InfoChecker = inv.Checker;
                t._tax.InfoSellerAddressPhone= inv.SellerAddress;
                t._tax.InfoSellerBankAccount= inv.SellerBank;
                //明细
                t._tax.ClearInvList();
                for (int i = 0; i < inv.Goods.Count; i++)
                {
                    t._tax.InvListInit();
                    t._tax.ListGoodsName = inv.Goods[i].GoodName;
                    t._tax.ListNumber = Convert.ToDouble(inv.Goods[i].GoodNumber);
                    t._tax.ListPrice = Convert.ToDouble(inv.Goods[i].GoodPrice);
                    t._tax.ListPriceKind = inv.Goods[i].PriceKind<0? (short)1 :inv.Goods[i].PriceKind;
                    t._tax.ListAmount = Convert.ToDouble(inv.Goods[i].Amount);
                    t._tax.ListUnit = inv.Goods[i].GoodUnit;
                    t._tax.ListPriceKind = 1;
                    string tData = "<?xml version=\"1.0\" encoding=\"GBK\"?>\r\n"
                    + "<FPXT>\r\n"
                    + "<INPUT>\r\n"
                    + "<GoodsNo>\r\n"
                    + "<GoodsNoVer>" + inv.Goods[i].TaxVer + "</GoodsNoVer>\r\n"
                    + "<GoodsTaxNo>" + inv.Goods[i].TaxCode + "</GoodsTaxNo>\r\n"
                    + (inv.TaxRate == 0 ? "<TaxPre>1</TaxPre>\r\n" : "<TaxPre>0</TaxPre>\r\n")
                    + (inv.TaxRate == 0 ? "<TaxPreCon>免税</TaxPreCon>\r\n" : "<TaxPreCon></TaxPreCon>\r\n")
                    + (inv.TaxRate == 0 ? "<ZeroTax>1</ZeroTax>\r\n": "<ZeroTax></ZeroTax>\r\n")
                    + "<CropGoodsNo>" + inv.Goods[i].GoodCode + "</CropGoodsNo>\r\n"
                    + "<TaxDeduction></TaxDeduction>\r\n"
                    + "</GoodsNo>\r\n</INPUT>\r\n</FPXT>";
                    tData = Convert.ToBase64String(Encoding.Default.GetBytes(tData));
                    string param = "<?xml version=\"1.0\" encoding=\"GBK\"?>\r\n"
                        + "<FPXT_COM_INPUT>\r\n"
                        + "<ID>1100</ID>\r\n"
                        + "<DATA>" + tData + "</DATA>\r\n"
                        + "</FPXT_COM_INPUT>";
                    string retmsg = t._tax.BatchUpload(param);
                    t._tax.AddInvList();
                    if (retmsg.IndexOf("<CODE>0000</CODE>") == -1)
                        throw new Exception("发票开具失败！\r\n错误信息:\r\n" + retmsg);
                }
                t._tax.Invoice();
                if (t._tax.RetCode != 4011)
                    throw new Exception("发票开具失败！\r\n错误代码:" + t._tax.RetCode + "\r\n错误信息:" + t._tax.RetMsg);
                t._tax.InfoKind = invType;
                t._tax.InfoTypeCode = invCode;
                t._tax.InfoNumber = invNumber;
                t._tax.PrintInv();
            }
            catch (Exception)
            {
                throw;
            }
        }
        internal void CancelInvoice(object tax, short invType, string invCode, string invNum)
        {
            try
            {
                TaxInv t = tax as TaxInv;
                if (null == t)
                    throw new Exception("未初始化！");
                t._tax.InfoKind = invType;
                t._tax.InfoTypeCode = invCode;
                t._tax.InfoNumber = Convert.ToInt32(invNum);
                t._tax.CancelInv();
                if (t._tax.RetCode != 6011)
                    throw new Exception("发票作废失败！\r\n错误代码:" + t._tax.RetCode + "\r\n错误信息:" + t._tax.RetMsg);
            }
            catch (Exception)
            {

                throw;
            }
        }
        internal void CloseCard(object tax)
        {
            try
            {
                TaxInv t = tax as TaxInv;
                t._tax.CloseCard();
            }
            catch (Exception)
            {
                throw;
            }

        }
    }
}
