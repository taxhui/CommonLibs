﻿using CommonModels.Invoice;
using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InvoiceComponent
{
    public class InvManage : IInvoice
    {
        bool IInvoice.CancelInv(string invCode, int invNumber, short invType)
        {
            throw new NotImplementedException();
        }

        void IInvoice.CloseCard()
        {
            throw new NotImplementedException();
        }

        InvStockModel IInvoice.GetInvStock(short invType)
        {
            return TaxInv.GetInvCode(invType);
        }

        InvErrModel IInvoice.GetLastError()
        {
            throw new NotImplementedException();
        }

        void IInvoice.Init(string certPass)
        {
            if (string.IsNullOrEmpty(certPass))
                throw new Exception("证书密码不能为空！");
            TaxInv.InitTaxCard(certPass);
        }

        bool IInvoice.MakeInvoice(InvInputModel inv, bool print)
        {
            throw new NotImplementedException();
        }

        void IInvoice.PrintInvoice(string invCode, int invNumber, short invType)
        {
            throw new NotImplementedException();
        }

        InvInfoModel IInvoice.QueryInvoice(string invCode, int invNumber, short invType)
        {
            throw new NotImplementedException();
        }
    }
}
