using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Network.Delegate
{
    //通知事件委托
    public delegate void NotifyEventHandler<Tevent, TSession>(Tevent e, TSession session);
}
