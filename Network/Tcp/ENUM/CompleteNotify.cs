using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Network.Tcp
{
    public enum CompleteNotify
    {
        OnConnected,
        OnSend,
        OnDataReceiveing,
        OnDataReceived,
        OnClosed,
    }
}
