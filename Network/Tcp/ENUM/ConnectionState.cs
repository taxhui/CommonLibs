using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Network.Tcp
{
    public enum ConnectionState
    {
        None = 0,
        Connecting = 1,
        Connected = 2,
        Closed = 3,
    }
}
