using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using Network.Delegate;
using Network.Tcp;
using Network.Tcp.NetSession;
using Network.Tcp.TcpConfig;

namespace Network.Tcp
{
    public class TcpFactory
    {
        public static ClientAgent CreateClientAgent(
            SessionType sessionType,
            ClientConfig cfg,
            NotifyEventHandler<CompleteNotify, Session> completetionNotify)
        {
            cfg._SessionIsService = false;
            return new ClientAgent(sessionType, cfg, completetionNotify);
        }

        public static ServerAgent CreateServerAgent(
            SessionType saeaSessionType,
            ServerConfig configuration,
            NotifyEventHandler<CompleteNotify, Session> completetionNotify)
        {
            configuration._SessionIsService = true;
            return new ServerAgent(saeaSessionType, configuration, completetionNotify);
        }
    }
}
