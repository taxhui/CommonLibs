using System;
using System.Net.Sockets;
using System.Threading;

namespace Network.Tcp.Awaitable
{
    public class NetHelper
    {
        #region SOCKET方法
        private static readonly Func<Socket, Awaiter, bool> ACCEPT = (s, a) => s.AcceptAsync(a.Args);
        private static readonly Func<Socket, Awaiter, bool> CONNECT = (s, a) => s.ConnectAsync(a.Args);
        private static readonly Func<Socket, Awaiter, bool> DISCONNECT = (s, a) => s.DisconnectAsync(a.Args);
        private static readonly Func<Socket, Awaiter, bool> RECEIVE = (s, a) => s.ReceiveAsync(a.Args);
        private static readonly Func<Socket, Awaiter, bool> SEND = (s, a) => s.SendAsync(a.Args);
        #endregion
        #region 方法封装
        public static void AcceptAsync(Socket socket, Awaiter awaiter, Action<Awaiter, SocketError> callback)
            => OperateAsync(socket, awaiter, ACCEPT, callback);
        public static void ConnectAsync(Socket socket, Awaiter awaiter, Action<Awaiter, SocketError> callback)
            => OperateAsync(socket, awaiter, CONNECT, callback);

        public static void DisonnectAsync(Socket socket, Awaiter awaiter, Action<Awaiter, SocketError> callback)
            => OperateAsync(socket, awaiter, DISCONNECT, callback);

        public static void ReceiveAsync(Socket socket, Awaiter awaiter, Action<Awaiter, SocketError> callback)
            => OperateAsync(socket, awaiter, RECEIVE, callback);

        public static void SendAsync(Socket socket, Awaiter awaiter, Action<Awaiter, SocketError> callback)
            => OperateAsync(socket, awaiter, SEND, callback);
        #endregion
        #region 执行
        private static void OperateAsync(Socket socket, Awaiter awaiter, Func<Socket, Awaiter, bool> operation, Action<Awaiter, SocketError> callback)
        {

            if (socket == null) return;

            try
            {
                awaiter.Reset();//清空上次的回调
                awaiter.OnCompleted(callback); //注册新的回调
                operation.Invoke(socket, awaiter);//执行指定的动作
            }
            catch
            {
                
            }

        }
        #endregion
    }
}
