using System;
namespace Network.Tcp.TcpConfig
{
    public interface INetConfig
    {
        int ReceiveBufferSize { get; set; }
        int SendBufferSize { get; set; }
        TimeSpan ReceiveTimeout { get; set; }
        TimeSpan SendTimeout { get; set; }
        bool NoDelay { get; set; }
        bool ReuseAddress { get; set; }
    }
    public class ConfigBase : INetConfig
    {
        public ConfigBase()
        {
            CompressTransferFromPacket = true;//是否压缩数据
            ReceiveBufferSize = 8192;
            SendBufferSize = 1024 * 1024 * 2;//默认2m，避免多次send，可根据自己的应用设置最优参数值
            ReceiveTimeout = TimeSpan.Zero;
            SendTimeout = TimeSpan.Zero;
            NoDelay = true;//是否开启nagle算法
            AppKeepAlive = true;//心跳包
            KeepAlive = false;
            KeepAliveInterval = 5000;
            KeepAliveSpanTime = 1000;
            ReuseAddress = true;//是否重用IP地址
        }

        /// <summary>
        /// 服务端标志
        /// </summary>
        internal bool _SessionIsService { get; set; }
        /// <summary>
        /// 数据压缩开关
        /// </summary>
        public bool CompressTransferFromPacket { get; set; }
        /// <summary>
        /// 接收缓冲区大小
        /// </summary>
        public int ReceiveBufferSize { get; set; }
        /// <summary>
        /// 发送缓冲区大小
        /// </summary>
        public int SendBufferSize { get; set; }
        /// <summary>
        /// 接收超时时间
        /// </summary>
        public TimeSpan ReceiveTimeout { get; set; }
        //发送超时时间
        public TimeSpan SendTimeout { get; set; }
        /// <summary>
        /// 是否使用nagle算法
        /// </summary>
        public bool NoDelay { get; set; }
        /// <summary>
        /// 是否启用应用层心跳
        /// </summary>
        public bool AppKeepAlive { get; set; }
        /// <summary>
        /// 启用心跳
        /// </summary>
        public bool KeepAlive { get; set; }
        /// <summary>
        /// 第一次心跳时间
        /// </summary>
        public int KeepAliveInterval { get; set; }
        /// <summary>
        /// 心跳间隔
        /// </summary>
        public int KeepAliveSpanTime { get; set; }
        /// <summary>
        /// 是否可以重用IP
        /// </summary>
        public bool ReuseAddress { get; set; }
    }
    public class ClientConfig : ConfigBase
    {

    }
    public class ServerConfig : ConfigBase
    {
        public ServerConfig()
        {
            PendingConnectionBacklog = 200;//服务器连接挂起队列数量
        }
        public int PendingConnectionBacklog { get; set; }
    }
}
