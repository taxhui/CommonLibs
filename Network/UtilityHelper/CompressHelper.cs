using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;

namespace Network.UtilityHelper
{
    class CompressHelper
    {
        #region 压缩
        public static byte[] Compress(byte[] data, int offset, int lenght)
        {
            if (null == data)
                return null;
            if (data.Length < 1)
                return null;
            byte[] buffer = null;
            try
            {
                MemoryStream ms = new MemoryStream();
                GZipStream zip = new GZipStream(ms, CompressionMode.Compress, true);
                zip.Write(data, offset, lenght);
                zip.Close();
                buffer = new byte[ms.Length];
                ms.Position = 0;
                ms.Read(buffer, 0, buffer.Length);
                ms.Close();
            }
            catch { }
            return buffer;
        }
        #endregion
        #region 解压
        public static byte[] Decompress(byte[] data)
        {
            if (null == data)
                return null;
            if (data.Length < 1)
                return null;
            byte[] buffer = null;
            try
            {
                MemoryStream ms = new MemoryStream(data);
                GZipStream zip = new GZipStream(ms, CompressionMode.Decompress, true);
                MemoryStream msreader = new MemoryStream();
                buffer = new byte[0x1000];
                while (true)
                {
                    int reader = zip.Read(buffer, 0, buffer.Length);
                    if (reader <= 0)
                    {
                        break;
                    }
                    msreader.Write(buffer, 0, reader);
                }
                zip.Close();
                ms.Close();
                msreader.Position = 0;
                buffer = msreader.ToArray();
                msreader.Close();
            }
            catch { }
            return buffer;
        }
        #endregion
    }
}
