﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonModels.Invoice
{
    public class InvStockModel
    {
        public string InvCode { get; set; }
        public int InvNumber { get; set; }
        public int StockCount { get; set; }
    }
}
