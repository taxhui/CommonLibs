﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonModels.Invoice
{
    public class InvInputModel
    {
        /// <summary>
        /// 购方信息
        /// </summary>
        public InvCustomModel CustomerInfo { get; set; }
        /// <summary>
        /// 销方信息
        /// </summary>
        public InvCustomModel SellerInfo { get; set; }
        /// <summary>
        /// 税率
        /// </summary>
        public short TaxRate { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 开票人
        /// </summary>
        public string Maker { get; set; }
        /// <summary>
        /// 收款人
        /// </summary>
        public string Payee { get; set; }
        /// <summary>
        /// 复核人
        /// </summary>
        public string Checker { get; set; }
        /// <summary>
        /// 明细列表
        /// </summary>
        public List<InvGood> Goods { get; set; }

    }
    public class InvCustomModel
    {
        /// <summary>
        /// 企业名
        /// </summary>
        public string CustName { get; set; }
        /// <summary>
        /// 银行账号
        /// </summary>
        public string CustBank { get; set; }
        /// <summary>
        /// 地址电话
        /// </summary>
        public string CustAddress { get; set; }
        /// <summary>
        /// 税号
        /// </summary>
        public string CustTax { get; set; }

    }
    public class InvGood
    {
        /// <summary>
        /// 是否含税0否1是
        /// </summary>
        public short PriceKind { get; set; }
        /// <summary>
        /// 商品名
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 计量单位
        /// </summary>
        public string Unit { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public decimal Quantity { get; set; }
        /// <summary>
        /// 单价
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// 金额
        /// </summary>
        public decimal Amount { get; set; }
        /// <summary>
        /// 税收分类编码版本号
        /// </summary>
        public string TaxCodeVersion { get; set; }
        /// <summary>
        /// 税收分类编码
        /// </summary>
        public string TaxCode { get; set; }
        /// <summary>
        /// 开票系统中的商品编码
        /// </summary>
        public string CompanyCode { get; set; }
    }
}
