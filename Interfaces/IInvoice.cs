﻿using CommonModels.Invoice;

namespace Interfaces
{
    public interface IInvoice
    {
        /// <summary>
        /// 开启金税盘
        /// </summary>
        /// <param name="certPass">证书密码</param>
        void Init(string certPass); //开启金税盘
        /// <summary>
        /// 读取库存信息
        /// </summary>
        /// <param name="invType">发票类型0专票2普票11货运12机动车</param>
        /// <returns></returns>
        InvStockModel GetInvStock(short invType); 
        /// <summary>
        /// 开具发票
        /// </summary>
        /// <param name="inv">发票信息</param>
        /// <param name="print">开票成功后是否直接打印</param>
        /// <returns></returns>
        bool MakeInvoice(InvInputModel inv, bool print);
        /// <summary>
        /// 打印发票
        /// </summary>
        /// <param name="invCode">发票代码</param>
        /// <param name="invNumber">发票号码</param>
        /// <param name="invType">发票类型0专票2普票11货运12机动车</param>
        void PrintInvoice(string invCode, int invNumber, short invType);
        /// <summary>
        /// 作废发票
        /// </summary>
        /// <param name="invCode">发票代码</param>
        /// <param name="invNumber">发票号码</param>
        /// <param name="invType">发票类型0专票2普票11货运12机动车</param>
        /// <returns></returns>
        bool CancelInv(string invCode, int invNumber, short invType);
        /// <summary>
        /// 发票查询
        /// </summary>
        /// <param name="invCode">发票代码</param>
        /// <param name="invNumber">发票号码</param>
        /// <param name="invType">发票类型0专票2普票11货运12机动车</param>
        /// <returns></returns>
        InvInfoModel QueryInvoice(string invCode, int invNumber, short invType);
        InvErrModel GetLastError();
        void CloseCard();
    }
}
