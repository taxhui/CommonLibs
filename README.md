# CommonLibs

#### 介绍

自己常用的一些工具

包含一个加密解密工具：AES/DES/3DES/MD5/SHA/RSA

一个数据库Helper:MSSQL/SQLITE

一个Http Post工具：没啥可说的，项目里要用，随便写了个很简单的，能发送请求接收结果，其它没了。

一个Gzip压缩类

一个Socket Tcp异步通信库，服务器客户端都可以用，修改自Simay.Socket

没了，就这些

#### 说明

其中Network是一个异步Socket库，由Simay.Socket改造而来，类名与原来不同了，修改了部分代码使其更适合我自己使用。

Simay.Socket 原版地址: https://gitee.com/dWwwang/SiMaySocket

Panuon.Silver有少量修改，总体没有什么变化 

Panuon.Siler 原版地址: https://github.com/Panuon/PanuonUI.Silver
