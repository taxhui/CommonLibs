﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Taxhui.Utils.UserModel
{
    public enum SqlDBConnType
    {
        SQL = 0,
        Windows = 1
    }
    public enum SqlDBProto
    {
        TCP_IP = 0,
        Named_Pipes = 1,
        RPC = 2,
        Banyan_Vines = 3,
        IPX_SPX = 4
    }
    public class SqlDbBatchModel
    {
        public string SqlStr { get; set; }
        public SqlParameter[] Param { get; set; }
    }
    public class SqlDBConfig
    {
        private int _loadBalanceTimeout = 60;
        private int _minPoolSize = 10;
        private int _maxPoolSize = 8192;
        private int _connTimeout = 30;
        private string _appName = "TaxhuiDBHelper";
        private string _dbConnLib = "dbmssocn";
        private SqlDBProto _dbProtocol = SqlDBProto.TCP_IP;
        private int _packetSize = 8192;
        public string DBServer { get; set; } = "127.0.0.1";
        public string DBUser { get; set; } = "sa";
        public string DBPass { get; set; } = "";
        public string DBName { get; set; } = "master";
        public int LoadBalanceTimeout
        {
            get { return _loadBalanceTimeout; }
            set
            {
                if (value < 0)
                    _loadBalanceTimeout = 0;
                else
                    _loadBalanceTimeout = value;
            }
        }
        public int MinPoolSize
        {
            get { return _minPoolSize; }
            set
            {
                if (value < 0)
                    _minPoolSize = 0;
                else
                    _minPoolSize = value;
            }
        }
        public int MaxPoolSize
        {
            get { return _maxPoolSize; }
            set
            {
                if (value < _minPoolSize)
                    _maxPoolSize = _minPoolSize;
                else
                    _maxPoolSize = value;
            }
        }
        public int ConnectionTimeout
        {
            get { return _connTimeout; }
            set
            {
                if (value < 1)
                    _connTimeout = 1;
                else
                    _connTimeout = value;
            }
        }
        public string AppName
        {
            get { return _appName; }
            set
            {
                if (string.IsNullOrEmpty(value))
                    _appName = "TaxhuiDBHelper";
                else
                    _appName = value;
            }
        }
        public SqlDBProto DBConnProtocolLib
        {
            get { return _dbProtocol; }
            set
            {
                _dbProtocol = value;
                switch (value)
                {
                    case SqlDBProto.TCP_IP:
                        _dbConnLib = "dbmssocn";
                        break;
                    case SqlDBProto.Named_Pipes:
                        _dbConnLib = "dbnmpntw";
                        break;
                    case SqlDBProto.RPC:
                        _dbConnLib = "dbmsrpcn";
                        break;
                    case SqlDBProto.Banyan_Vines:
                        _dbConnLib = "dbmsvinn";
                        break;
                    case SqlDBProto.IPX_SPX:
                        _dbConnLib = "dbmsspxn";
                        break;
                    default:
                        _dbConnLib = "dbmssocn";
                        break;
                }
            }
        }
        public SqlDBConnType ConnType { get; set; } = SqlDBConnType.SQL;
        public int PacketSize
        {
            get { return _packetSize; }
            set
            {
                if (_packetSize < 128)
                    _packetSize = 128;
                else
                    _packetSize = value;
            }
        }
        public bool Pooling { get; set; } = true;
        public string ConnStr
        {
            get
            {
                if (ConnType == SqlDBConnType.SQL)
                    return $"Application Name={_appName};Server={DBServer};Database={DBName};Integrated Security=False;Net={_dbConnLib};User ID={DBUser};Password={DBPass};Min Pool Size={_minPoolSize};Max Pool Size={_maxPoolSize};Connect Timeout={_connTimeout};Load Balance Timeout={_loadBalanceTimeout};Packet Size={_packetSize};Pooling={(Pooling ? "true" : "false")}";
                else
                    return $"Application Name={_appName};Server={DBServer};Database={DBName};Integrated Security=SSPI;Min Pool Size={_minPoolSize};Max Pool Size={_maxPoolSize};Connect Timeout={_connTimeout};Load Balance Timeout={_loadBalanceTimeout};Packet Size={_packetSize};Pooling={(Pooling ? "true" : "false")}";
            }
        }

    }
}
