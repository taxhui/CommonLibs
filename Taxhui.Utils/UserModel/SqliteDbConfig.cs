﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taxhui.Utils.UserModel
{
    public class SqliteDbConfig
    {
        public string DBpath { get; set; }
        public string Password { get; set; }
        public string DBVersion { get; set; } = "3";
        public bool ReadOnlyConnection { get; set; } = false;
        public bool Pooling { get; set; } = true;
        public int PoolSize { get; set; } = 1024;
        public int CacheSize { get; set; } = 8192;
        public int PageSize { get; set; } = 4096;
        public string ConnStr
        {
            get
            {
                string _connStr = $"Data Source={DBpath};";
                _connStr += $"Version={DBVersion};";
                _connStr += string.IsNullOrWhiteSpace(Password) ?"": $"Password={Password};" ;
                _connStr += $"Read Only={(ReadOnlyConnection ? "True" : "False")};";
                _connStr += Pooling ? $"Pooling=True;Max Pool Size={PoolSize};" : "";
                _connStr += $"Cache Size={CacheSize};";
                _connStr += $"Page Size={PageSize}";
                return _connStr;
            }
        }
    }
}
