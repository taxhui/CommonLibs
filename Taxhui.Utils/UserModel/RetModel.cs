﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taxhui.Utils.UserModel
{
    /// <summary>
    /// 服务器返回模型
    /// </summary>
    public class RetModel
    {
        /// <summary>
        /// 成功标识
        /// </summary>
        public bool Success { get; set; }
        /// <summary>
        /// 错误代码
        /// </summary>
        public string ErrCode { get; set; }
        /// <summary>
        /// 错误信息
        /// </summary>
        public string ErrInfo { get; set; }
        /// <summary>
        /// 返回数据
        /// </summary>
        public object Data { get; set; }
    }
}
