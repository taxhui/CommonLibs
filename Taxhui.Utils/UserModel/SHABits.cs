﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taxhui.Utils.UserModel
{
    public enum SHABits
    {
        sha1,
        sha256,
        sha384,
        sha512
    }
}
