﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taxhui.Utils.Network.CustomProc
{
    public interface ICustomDataProc
    {
        byte[] ResolvePack(byte[] header,byte[] data); //打包
        bool IsAutoHeader(); //是否需要自动加包头
        int GetMaxHeadLen(); //包头最大长度
        int ResolveHead(byte[] header);//包头是否已接收完,完成返回包体大小否则0
    }
}
