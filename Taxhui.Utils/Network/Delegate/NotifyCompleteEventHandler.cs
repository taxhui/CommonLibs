namespace Taxhui.Utils.Network.Delegate
{
    //通知事件委托
    public delegate void NotifyEventHandler<Tevent, TSession>(Tevent e, TSession session);
}
