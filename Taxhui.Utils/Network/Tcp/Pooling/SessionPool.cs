using System;
using Taxhui.Utils.Network.Tcp.NetSession;

namespace Taxhui.Utils.Network.Tcp.Pooling
{
    public class SessionPool : ObjectPool<Session>
    {
        private Func<Session> _createMethod;
        private Action<Session> _cleanMethod;
        #region 初始会话池
        public SessionPool Initialize(Func<Session> createMethod, Action<Session> cleanMethod, int initialCount = 0)
        {
            //创建对象的方法
            _createMethod = createMethod ?? throw new ArgumentNullException("createSaea");
            //清理对象的方法
            _cleanMethod = cleanMethod ?? throw new ArgumentNullException("cleanSaea");
            //初始数量不能小于0
            if (initialCount < 0)
                throw new ArgumentOutOfRangeException(
                    "initialCount",
                    initialCount,
                    "初始数量不能小于0");

            for (int i = 0; i < initialCount; i++)
            {
                Add(Create());
            }
            return this;
        }
        #endregion
        #region 创建对象
        protected override Session Create()
        {
            return _createMethod();
        }
        #endregion
        #region 清理并返还对象
        public void Return(Session saea)
        {
            _cleanMethod(saea);
            Add(saea);
        }
        #endregion
    }
}
