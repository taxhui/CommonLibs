using Taxhui.Utils.Network.Delegate;
using Taxhui.Utils.Network.Tcp.Pooling;
using Taxhui.Utils.Network.Tcp.TcpConfig;
using Taxhui.Utils.Network.UtilityHelper;
using System;
using System.Net.Sockets;
using Taxhui.Utils.Network.CustomProc;

namespace Taxhui.Utils.Network.Tcp.NetSession
{
    public abstract class Session : ICustomDataProc
    {
        protected readonly NotifyEventHandler<CompleteNotify, Session> _notifyEventHandler;
        protected readonly ConfigBase _configuration;
        protected readonly AwaiterPool _awaiterPool;
        protected readonly SessionPool _sessionPool;
        protected readonly LogHelper _logger;

        protected readonly NetEngineBased _agent;
        protected readonly object _opsLock = new object();
        protected ConnectionState _state;
        protected byte[] _completebuffer;
        protected Socket _socket;
        protected DateTime _startTime;

        protected ICustomDataProc _headerProc;

        public string SessionId { get; private set; }
        public int SendTransferredBytes { get; protected set; }
        public int ReceiveBytesTransferred { get; protected set; }
        public byte[] CompletedBuffer { get { return _completebuffer; } }
        public object[] AppTokens { get; set; }
        public Socket Socket { get { return _socket; } }
        public bool Connected { get { return _socket != null && _socket.Connected; } }
        public ConnectionState State { get { return _state; } internal set { _state = value; } }
        public DateTime StartTime { get { return _startTime; } }

        internal Session(
            NotifyEventHandler<CompleteNotify, Session> notifyEventHandler,
            ConfigBase configuration,
            AwaiterPool handlerSaeaPool,
            SessionPool sessionPool,
            NetEngineBased agent,
            LogHelper logHelper,
            ICustomDataProc headerProc)
        {
            _notifyEventHandler = notifyEventHandler;
            _configuration = configuration;
            _awaiterPool = handlerSaeaPool;
            _sessionPool = sessionPool;
            _agent = agent;
            _logger = logHelper;
            _headerProc = headerProc ?? this;
            //�������ID
            string id = Guid.NewGuid().ToString();
            SessionId = id;
        }

        internal abstract void Attach(Socket socket);

        internal abstract void Detach();

        internal abstract void StartProcess();

        public abstract void SendAsync(byte[] data);

        public abstract void SendAsync(byte[] data, int offset, int lenght);

        public abstract void Close(bool notify);

        public byte[] ResolvePack(byte[] header, byte[] data)
        {
            int hd = BitConverter.ToInt32(header, 0);
            Array.Resize(ref data, hd);
            return data;
        }

        public bool IsAutoHeader()
        {
            return true;
        }

        public int ResolveHead(byte[] header)
        {
            if (header.Length >= 4)
                return BitConverter.ToInt32(header, 0);
            return -1;
        }

        public int GetMaxHeadLen()
        {
            return 4;
        }
    }
}