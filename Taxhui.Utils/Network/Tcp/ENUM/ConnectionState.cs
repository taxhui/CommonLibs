namespace Taxhui.Utils.Network.Tcp
{
    public enum ConnectionState
    {
        None = 0,
        Connecting = 1,
        Connected = 2,
        Closed = 3,
    }
}
