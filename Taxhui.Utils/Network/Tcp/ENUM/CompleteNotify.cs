namespace Taxhui.Utils.Network.Tcp
{
    public enum CompleteNotify
    {
        OnConnected,
        OnSend,
        OnDataReceiveing,
        OnDataReceived,
        OnClosed,
    }
}
