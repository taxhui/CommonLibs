using Taxhui.Utils.Network.CustomProc;
using Taxhui.Utils.Network.Delegate;
using Taxhui.Utils.Network.Tcp.NetSession;
using Taxhui.Utils.Network.Tcp.TcpConfig;

namespace Taxhui.Utils.Network.Tcp
{
    public class TcpFactory
    {
        public static ClientAgent CreateClientAgent(
            SessionType sessionType,
            ClientConfig cfg,
            NotifyEventHandler<CompleteNotify, Session> completetionNotify,
            ICustomDataProc hdProc=null)
        {
            cfg._SessionIsService = false;
            return new ClientAgent(sessionType, cfg, completetionNotify,hdProc);
        }

        public static ServerAgent CreateServerAgent(
            SessionType saeaSessionType,
            ServerConfig configuration,
            NotifyEventHandler<CompleteNotify, Session> completetionNotify)
        {
            configuration._SessionIsService = true;
            return new ServerAgent(saeaSessionType, configuration, completetionNotify);
        }
    }
}
